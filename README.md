# README #

This README would normally document whatever steps are necessary to get your application up and running.

### The All Convolutional Net ###

* This is repo is the implementation of The All Convolutional Net Paper 
(http://arxiv.org/abs/1412.6806)
Version 0.1

_Ciphar-10 and Ciphar-100_ image datasets are used

### Dependencies ###

``` pip -r install requirements.txt``` will install the dependancies listed in the requirements.txt file

### Contribution guidelines ###

To run experiments, run the experiments.notebook.ipynb notebook and follow the examples to train each type of model

### Contributors ###

Elias Hussen and Dan Rosenthal